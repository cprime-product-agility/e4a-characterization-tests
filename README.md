# e4a-characterization-tests

A codebase for explaining and demonstrating the concept of _characterization tests_ (CTs). The idea was [coined by Michael Feathers](https://michaelfeathers.silvrback.com/characterization-testing) as a technique for documenting the behavior of existing code. The purpose is to show _what the code currently does_, not what it should or should not do. Thus, CTs can result in documenting buggy or unexpected/undesireable behavior.

**The reason for writing characterization tests is to create a safety net for upcoming changes. It is a precursor for safely eliminating technical debt.**

The word "safe" is used here on purpose, because after CTs are in place, we can then change existing code and immediately know whether or not it still behaves the way it did originally.

## Getting started (for class participants)

1. Clone this repo locally.
2. Switch to the branch containing your preferred language/tech stack.
3. Watch the demonstration of how to add a characterization test.
4. Add more of your own.

**Sample inputs to exercise the code with:**

* A string containing a fake SSN.
* A string containing a fake credit card number.
* A string containing a fake e-mail address.
* A number representing a fake SSN.
* A number representing a fake credit card number.
* A number that starts with 0.
* A negative number.
* The `null` (or `nil`) value.
* An empty string.

## Note to maintainers

This branch structure was specifically chosen to facilitate multiple languages and tech stacks. Do not merge anything back to `main`, nor add any code to `main`. `main` is only meant to store documentation.

## License

Copyright 2022 Cprime, Inc. All rights are reserved.
